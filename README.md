# EP1 - OO (UnB - Gama)

Este projeto consiste em um programa em C++ capaz de aplicar filtros em imagens de formato `.ppm`.
Utilize o comando make para compilar o programa:
$ make
Para executar, utilize:
$ make run
Selecione uma opcao que deseja aplicar no arquivo:
    (1) - Filtro Preto e Branco
    (2) - Filtro Polarizado
    (3) - Filtro Negativo
    (4) - Copiar Imagem
    (5) - Fechar Programa

Digite o nome da imagem; > Exemplos:
    imagem1
    imagem2
Digite o nome da imagem a ser salvada; > Exemplos:
    frutaspolarizadas.ppm
    floresnegativo.ppm
