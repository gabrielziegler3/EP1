#ifndef IMAGEM_HPP
#define IMAGEM_HPP
#include <iostream>
#include <string>
#include <cstdio>
#include <fstream>
#include <cstdlib>

using namespace std;

class Imagem {

private:
	string tipo;
	unsigned int largura, altura, cor_max;

public:


	ofstream arquivo_filtrado;
	Imagem();
	~Imagem();
	Imagem(string tipo, unsigned int largura, unsigned int altura, unsigned int cor_max);
	string getTipo();
	void setTipo(string tipo);
	unsigned getLargura();
	void setLargura(unsigned int largura);
	unsigned int getAltura();
	void setAltura(unsigned int altura);
	unsigned int getCorMax();
	void setCorMax(unsigned int cor_max);

	void salvaDados(ofstream &arquivo_filtrado);
	virtual	void salvaPixel(ofstream &arquivo_filtrado);
	unsigned char **matrizR, **matrizG, **matrizB;
};
#endif
