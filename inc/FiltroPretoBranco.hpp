#ifndef FILTROPRETOBRANCO_HPP
#define FILTROPRETOBRANCO_HPP

#include "Imagem.hpp"

using namespace std;

class FiltroPretoBranco : public Imagem {
	public:
		FiltroPretoBranco();
		~FiltroPretoBranco();
		FiltroPretoBranco(int largura, int altura, int cor_max);
		void salvaPixel(ofstream &arquivo_filtrado);
};

#endif
