#ifndef FILTROPOLARIZADO_HPP
#define FILTROPOLARIZADO_HPP

#include "Imagem.hpp"

using namespace std;

class FiltroPolarizado : public Imagem {
	public:
		FiltroPolarizado();
		~FiltroPolarizado();
		FiltroPolarizado(int largura, int altura, int cor_max);
		void salvaPixel(ofstream &arquivo_filtrado);
};

#endif
