#ifndef FILTRODEMEDIA_HPP
#define FILTRODEMEDIA_HPP

#include "Imagem.hpp"

using namespace std;

class FiltroDeMedia : public Imagem {
	public:
		FiltroDeMedia();
		~FiltroDeMedia();
		FiltroDeMedia(int largura, int altura, int cor_max);
		void salvaPixel(ofstream &arquivo_filtrado);
};
#endif
