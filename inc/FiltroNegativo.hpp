#ifndef FILTRONEGATIVO_HPP
#define FILTRONEGATIVO_HPP

#include "Imagem.hpp"

using namespace std;

class FiltroNegativo : public Imagem {
	public:
		FiltroNegativo();
		~FiltroNegativo();
		FiltroNegativo(int largura, int altura, int cor_max);
		void salvaPixel(ofstream &arquivo_filtrado);
};

#endif
