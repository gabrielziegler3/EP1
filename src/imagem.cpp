#include "Imagem.hpp"
#include <fstream>
#include <cstdio>
#include <iostream>
#include <cstring>

using namespace std;

Imagem::Imagem(){

    tipo = "";
    largura = 0;
    altura = 0;
    cor_max = 255;

    matrizR = new unsigned char*[altura];
    for(unsigned int i = 0; i < altura; i++)
    matrizR[i] = new unsigned char[largura];

    matrizG = new unsigned char*[altura];
    for(unsigned int i = 0; i < altura; i++)
    matrizG[i] = new unsigned char[largura];

    matrizB = new unsigned char*[altura];
    for(unsigned int i = 0; i < altura; i++)
    matrizB[i] = new unsigned char[largura];
}

Imagem::~Imagem(){

}

Imagem::Imagem(string tipo, unsigned int largura, unsigned int altura, unsigned int cor_max){
    this->tipo = tipo;
    this->largura = largura;
    this->altura = altura;
    this->cor_max = cor_max;
}

string Imagem::getTipo(){
    return tipo;
}

void Imagem::setTipo(string tipo){
    this->tipo = tipo;
}

unsigned int Imagem::getLargura(){
    return largura;
}

void Imagem::setLargura(unsigned largura){
    this->largura = largura;
}

unsigned Imagem::getAltura(){
    return altura;
}

void Imagem::setAltura(unsigned altura){
    this->altura = altura;
}

unsigned Imagem::getCorMax(){
    return cor_max;
}

void Imagem::setCorMax(unsigned cor_max){
    this->cor_max = cor_max;
}

void Imagem::salvaDados(ofstream &arquivo_filtrado){

    ifstream arquivo1;
    string nomedoarquivo;

    cout << "Digite a imagem que você deseja utilizar para aplicar o filtro: ";
    cin >> nomedoarquivo;

    nomedoarquivo = "./doc/" + nomedoarquivo + ".ppm";

    arquivo1.open(nomedoarquivo.c_str(), ios::binary);

    if(!arquivo1.is_open())
    {

    cout << "Erro ao abrir arquivo." << endl;
    return;
    }
    else
    {
    string tipo, comentario, comentarioaux;
    unsigned largura, altura, cor_max;

    getline(arquivo1,tipo);

    if(tipo != "P6")
    {
    cout << "Tipo de arquivo inválido!" << endl;
    return;
    }

    while(1)
    {
    getline(arquivo1,comentario);

    if(comentario[0] != '#')
    {
    int tamanho = comentario.length()+1;
    arquivo1.seekg(-tamanho,ios_base::cur);
    arquivo1 >> largura;
    arquivo1 >> altura;
    arquivo1 >> cor_max;
    break;
    }

    }

    setTipo(tipo);
    setLargura(largura);
    setAltura(altura);
    setCorMax(cor_max);

    arquivo1.seekg(1,ios_base::cur);

    string nomedoarquivosalvo;

    cout << "Digite o nome de como deseja salvar a nova imagem: " << endl;
    cin >> nomedoarquivosalvo;

    nomedoarquivosalvo = "./doc/" + nomedoarquivosalvo + ".ppm";

    while(nomedoarquivosalvo == nomedoarquivo)
    {
    cout << "\nO nome não pode ser igual ao da imagem original. Por favor, insira um novo nome: ";
    cin >> nomedoarquivosalvo;
    }

    arquivo_filtrado.open(nomedoarquivosalvo.c_str(),ios::binary);

    if(!arquivo_filtrado.is_open()){
    cout << "Erro na criação do novo arquivo!" << endl;
    return;
    }

    arquivo_filtrado << tipo << endl;
    arquivo_filtrado << largura << " " << altura << endl;
    arquivo_filtrado << cor_max << endl;

    unsigned int j, k;
    char r, g, b;

    matrizR = new unsigned char*[altura];
    for(unsigned int i = 0; i < altura; i++)
    matrizR[i] = new unsigned char[largura];

    matrizG = new unsigned char*[altura];
    for(unsigned int i = 0; i < altura; i++)
    matrizG[i] = new unsigned char[largura];

    matrizB = new unsigned char*[altura];
    for(unsigned int i = 0; i < altura; i++)
    matrizB[i] = new unsigned char[largura];


    for(j = 0; j < altura; j++)
    {
        for(k = 0; k < largura; k++)
        {
        arquivo1.get(r);
        arquivo1.get(g);
        arquivo1.get(b);
        matrizR[j][k] = r;
        matrizG[j][k] = g;
        matrizB[j][k] = b;
        }
    }
    }

    salvaPixel(arquivo_filtrado);

    arquivo1.close();
    arquivo_filtrado.close();
}

void Imagem::salvaPixel(ofstream &arquivo_filtrado){


    unsigned int **Red, **Green, **Blue;

    Red = new unsigned int*[altura];
    for(unsigned int i = 0; i < altura; i++)
    Red[i] = new unsigned int[largura];

    Green = new unsigned int*[altura];
    for(unsigned int i = 0; i < altura; i++)
    Green[i] = new unsigned int[largura];

    Blue = new unsigned int*[altura];
    for(unsigned int i = 0; i < altura; i++)
    Blue[i] = new unsigned int[largura];


    unsigned int i, j;

    for(i = 0; i < altura; i++)
    {
        for(j = 0; j < largura; j++)
        {
        Red[i][j] = (unsigned int)matrizR[i][j];
        arquivo_filtrado << (char)Red[i][j];
        Green[i][j] = (unsigned int)matrizG[i][j];
        arquivo_filtrado << (char)Green[i][j];
        Blue[i][j] = (unsigned int)matrizB[i][j];
        arquivo_filtrado << (char)Blue[i][j];
        }
    }
}
