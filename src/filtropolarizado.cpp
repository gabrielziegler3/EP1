#include "FiltroPolarizado.hpp"

using namespace std;

FiltroPolarizado::FiltroPolarizado(){

  setLargura(0);
  setAltura(0);
  setCorMax(0);

}

FiltroPolarizado::~FiltroPolarizado(){

}

FiltroPolarizado::FiltroPolarizado(int largura, int altura, int cor_max){
  setLargura(largura);
  setAltura(altura);
  setCorMax(cor_max);

}

void FiltroPolarizado::salvaPixel(ofstream &arquivo_filtrado){

  int largura, altura, cor_max;

  largura = getLargura();
  altura = getAltura();
  cor_max = getCorMax();

  int Red, Green, Blue;

  int i, j;

  for(i = 0; i < altura; i++)
  {
    for(j = 0; j < largura; j++)
    {
    Red = matrizR[i][j];
    if(Red > cor_max/2)
    matrizR[i][j] = cor_max;
    else
    matrizR[i][j] = 0;

    Green = matrizG[i][j];
    if(Green > cor_max/2)
    matrizG[i][j] = cor_max;
    else
    matrizG[i][j] = 0;

    Blue = matrizB[i][j];
    if(Blue > cor_max/2)
    matrizB[i][j] = cor_max;
    else
    matrizB[i][j] = 0;

    arquivo_filtrado << (char)matrizR[i][j];
    arquivo_filtrado << (char)matrizG[i][j];
    arquivo_filtrado << (char)matrizB[i][j];
    }
  }
}
