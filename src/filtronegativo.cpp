#include "FiltroNegativo.hpp"
#include <cstdio>
#include <cstdlib>

using namespace std;

FiltroNegativo::FiltroNegativo(){

	setLargura(0);
	setAltura(0);
	setCorMax(0);
}

FiltroNegativo::~FiltroNegativo(){

}

FiltroNegativo::FiltroNegativo(int largura, int altura, int cor_max){
	setLargura(largura);
	setAltura(altura);
	setCorMax(cor_max);
}

void FiltroNegativo::salvaPixel(ofstream &arquivo_filtrado){

	int largura, altura, cor_max;

	largura = getLargura();
	altura = getAltura();
	cor_max = getCorMax();

	int i=0, j=0;

	for(i = 0; i < altura; i++)
	{
		for(j = 0; j < largura; j++)
		{
		matrizR[i][j] = cor_max - matrizR[i][j];
		arquivo_filtrado << matrizR[i][j];

		matrizG[i][j] = cor_max - matrizG[i][j];
		arquivo_filtrado << matrizG[i][j];

		matrizB[i][j] = cor_max - matrizB[i][j];
		arquivo_filtrado << matrizB[i][j];
		}
	}
}
