#include "FiltroPretoBranco.hpp"

using namespace std;

FiltroPretoBranco::FiltroPretoBranco(){

	setLargura(0);
	setAltura(0);
	setCorMax(0);
}

FiltroPretoBranco::~FiltroPretoBranco(){

}

FiltroPretoBranco::FiltroPretoBranco(int largura, int altura, int cor_max){
	setLargura(largura);
	setAltura(altura);
	setCorMax(cor_max);
}

void FiltroPretoBranco::salvaPixel(ofstream &arquivo_filtrado){

	int largura, altura, cor_max;

	largura = getLargura();
	altura = getAltura();
	cor_max = getCorMax();

	int i, j;

	int grey;

	for(i = 0; i < altura; i++)
	{
		for(j = 0; j < largura; j++)
		{
		grey = (0.299*matrizR[i][j]) + (0.587*matrizG[i][j]) + (0.144*matrizB[i][j]);
		if(grey > cor_max)
		{
		matrizR[i][j] = cor_max;
		matrizG[i][j] = cor_max;
		matrizB[i][j] = cor_max;
		arquivo_filtrado << matrizR[i][j];
		arquivo_filtrado << matrizG[i][j];
		arquivo_filtrado << matrizB[i][j];
		}
		else
		{
		matrizR[i][j] = (char)grey;
		matrizG[i][j] = (char)grey;
		matrizB[i][j] = (char)grey;
		arquivo_filtrado << matrizR[i][j];
		arquivo_filtrado << matrizG[i][j];
		arquivo_filtrado << matrizB[i][j];
		}
		}
	}
}
