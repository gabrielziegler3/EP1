#include "Imagem.hpp"
#include "FiltroNegativo.hpp"
#include "FiltroPolarizado.hpp"
#include "FiltroPretoBranco.hpp"

int main (){

	int escolhaOpcao;
		cout << "Universidade de Brasília" << endl;
		cout << "Escolha uma das opções abaixo: " << endl;

		cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n" << endl;

		cout << "(1) - Filtro Preto e Branco" << endl;
		cout << "(2) - Filtro Polarizado" << endl;
		//cout << "(x) - Filtro de Média" << endl;
		cout << "(3) - Filtro Negativo" << endl;
		cout << "(4) - Copiar Imagem" << endl;
		cout << "(5) - Fechar Programa\n" << endl;
		cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" << endl;
		cout << "Gabriel Gomes Ziegler\n150166320\nOpcao:" << endl;


		cin >> escolhaOpcao;

		Imagem *imagem = new Imagem();
		FiltroNegativo *negativo = new FiltroNegativo();
		FiltroPolarizado *polarizado = new FiltroPolarizado();
		FiltroPretoBranco *pretobranco = new FiltroPretoBranco();

		ofstream arquivo_filtrado;

		system("clear");
	switch(escolhaOpcao){
		case 0:
			break;
		case 1:
			pretobranco->salvaDados(arquivo_filtrado); // preto e branco
			break;
		case 2:
			polarizado->salvaDados(arquivo_filtrado); // polarizado
			break;
		case 3:
			negativo->salvaDados(arquivo_filtrado); // negativo
			break;
		case 4:
			imagem->salvaDados(arquivo_filtrado); //copia
			break;
		case 5:
			break;
		default:
			system("clear");
			cout << "** Entrada invalida! **" << endl;
	}
	return 0;
}
